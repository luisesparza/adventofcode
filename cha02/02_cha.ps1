#$file = Get-Content ..\src\02_test.txt
$file = Get-Content ..\src\02_data.txt
$horizontal = 0
$depth = 0
$aim = 0

foreach ($o in $file) {
	if ($o.StartsWith("f"))  {
		$horizontal += [int]$o.split()[-1]
		$aim += $depth * [int]$o.split()[-1]
	}
	if ($o.StartsWith("u"))  {
		$depth -= [int]$o.split()[-1]
	}
	if ($o.StartsWith("d"))  {
		$depth += [int]$o.split()[-1]
	}
}

Write-Host ($horizontal * $depth)
Write-Host ($horizontal * $aim)