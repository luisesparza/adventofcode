#$file = Get-Content ..\src\04_test.txt
$file = Get-Content ..\src\04_data.txt
$blanks = 0
$matrixsize = 0
$premio = @()
$game = @()
$numatrix = [System.Collections.ArrayList]::new()

ForEach ($line in $file) { If ($line.length -eq 0) {$blanks++ } }

ForEach ($element in ($file | Select-Object -Index 2) -split " ") { 
	If ($element -ne "") { 
		$matrixsize++
	}
}

ForEach ($element in ($file | Select-Object -First 1) -split ",") { $premio += $element }

ForEach ($line in ($file | Select-Object -Skip 1) -ne "") {
	$arrayline = @()
	$lin++
	ForEach ($element in ($line -split " ") -ne "") {
		$arrayline += @($element)
	}
	$arraymatrix += @(,@($arrayline))
	if ($lin -eq $matrixsize){
		$game += @(,@($arraymatrix))
		$arraymatrix = @()
		$lin = 0
	}
}

function replace {
	Param (
	 	[Parameter(Mandatory=$true)] $rep,
		[Parameter(Mandatory=$true)] $mat
	)
	For ($m=0; $m -lt $blanks; $m++) {
		For ($l=0; $l -lt $matrixsize; $l++) {
			For ($n=0; $n -lt $matrixsize; $n++) {
				If ($mat[$m][$l][$n] -eq $rep) {
					$mat[$m][$l][$n] = "xx"
				}
			}
		}
	}
	return $mat
}

function win {
	Param (
		[Parameter(Mandatory=$true)] $mat
	)
	$prize = 0
	For ($l=0; $l -lt $matrixsize; $l++) {
		For ($n=0; $n -lt $matrixsize; $n++) {
			If ($mat[$l][$n] -eq "xx") {
				$prize++
			}
			If ($prize -eq 5) {
				return "premio"
			}
		}
		$prize = 0
		For ($n=0; $n -lt $matrixsize; $n++) {
			If ($mat[$n][$l] -eq "xx") {
				$prize++
			}
			If ($prize -eq 5) {
				return "premio"
			}
		}
		$prize = 0
	}
	return "no"
}

function prize {
	Param (
		[Parameter(Mandatory=$true)] $mat
	)
	$res = 0
	For ($l=0; $l -lt $matrixsize; $l++) {
		For ($n=0; $n -lt $matrixsize; $n++) {
			If ($mat[$l][$n] -ne "xx") {
				$res += $mat[$l][$n]
			}
		}
	}
	return $res	
}

For ($i=0; $i -lt $blanks; $i++){ [void]$numatrix.Add($i) } 

ForEach ($number in $premio) {
	For ($m=0; $m -lt $numatrix.count; $m++) {
		$game = replace $number $game
		$juego = win $game[$numatrix[$m]]
		If (($juego -eq "premio") -And ($numatrix.count -eq $blanks)) {
			$aux01 = prize $game[$numatrix[$m]]
			Write-Host ($aux01 * $number)
		}
		If (($juego -eq "premio") -And ($numatrix.count -eq 1)) {
			$aux02 = prize $game[$numatrix[$m]]
			Write-Host ($aux02 * $number)
			exit
		}
		If (($juego -eq "premio") -And ($numatrix.count -gt 1)) {
			$numatrix.RemoveAt($m)
		}
	}
}