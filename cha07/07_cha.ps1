$TestData = $false
$file = $TestData ? '..\src\07_test.txt' : '..\src\07_data.txt'
[int[]]$horizontal = (Get-Content -Path $file) -Split (',')
$minvalue = [int]($horizontal | Measure-Object -Minimum).Minimum
$maxvalue = [int]($horizontal | Measure-Object -Maximum).Maximum
[int[]]$crab = $minvalue..$maxvalue
[int[]]$fuel = $minvalue..$maxvalue

For ($i=0; $i -lt $crab.length; $i++) {
    $moves = 0
    $contador = 0
    Foreach ($number in $horizontal) {
        $cave = ($number - $i)
        $cave = ($cave -ge 0) ? $cave : -$cave
        $vueltas = 0
        For ($j=0; $j -lt $cave; $j++) {
            $vueltas += $j + 1
        }
        $moves += $cave
        $contador += $vueltas
    }
    $crab[$i] = $moves
    $fuel[$i] = $contador
}

Write-Host ($crab | Measure-Object -Minimum).Minimum
Write-Host ($fuel | Measure-Object -Minimum).Minimum