#$file = Get-Content ..\src\01_test.txt
$file = Get-Content ..\src\01_data.txt
$count01 = 0
$count02 = 0
$compara = $file | Select-Object -First 1
$window = 4

$file | Select-Object -Skip 1 | ForEach-Object {
	if($_ -match $regex){
	if([int]$_ -gt [int]$compara) {
		$count01++
	}
	$compara = $_
    }
}

for ($i = 0; $i -lt $file.length - $window + 2; $i++) {
	$a = [int]$file[$i] + [int]$file[$i+1] + [int]$file[$i+2]
	$b = [int]$file[$i+1] + [int]$file[$i+2] + [int]$file[$i+3]
	if ($b -gt $a) {
		$count02++
	}
}

Write-Host $count01
Write-Host $count02