#$file = Get-Content ..\src\05_test.txt
$file = Get-Content ..\src\05_data.txt
$overdos = 0
$matrixsize = 1000
$vin = [System.Collections.ArrayList]@()
$vout = [System.Collections.ArrayList]@()
$diagram = foreach ($i in 1..$matrixsize) { ,(,0 * $matrixsize) }

ForEach ($line in $file -split " -> ") {
	$value = ($line -split ",")
	If ( !($i % 2) ) {
		$vin += (@{'x'=$value[0]; 'y'=$value[1]})
	}
	Else {
		$vout += (@{'x'=$value[0]; 'y'=$value[1]})
	}
    $i++
}

For ($i=0; $i -lt $file.length; $i++) {
	[int]$x1 = $vin.x[$i]
	[int]$y1 = $vin.y[$i]
	[int]$x2 = $vout.x[$i]
	[int]$y2 = $vout.y[$i]
	if (($x1 -eq $x2) -and ($y1 -ne $y2)) {
		Foreach ($y in $y1..$y2) {
			$diagram[$x1][$y]++
		}
	}
	elseif (($y1 -eq $y2) -and ($x1 -ne $x2)) {
		Foreach ($x in $x2..$x1) {
			$diagram[$x][$y1]++

		}
	}
	elseif (($x1 -ne $x2) -and ($y1 -ne $y2))  {
		if ($x1 -gt $x2)  {
			$x1,$y1,$x2,$y2 = $x2,$y2,$x1,$y1
		}
		$x = $x1
		$y = $y1
		if ($y1 -gt $y2) {
			while ($x -le $x2) {
				$diagram[$x][$y]++
				$x++
				$y--
			}
		}
		elseif ($y1 -lt $y2) {
			while ($x -le $x2) {
				$diagram[$x][$y]++
				$x++
				$y++
			}
		}
	}
	elseif (($y1 -eq $y2) -and ($x1 -eq $x2)) {
		Write-Host "iguales" $i " " $x1 "" $y1 " " $x2 "" $y2
	}
	else {
		Write-Host "error" $i " " $x1 "" $y1 " " $x2 "" $y2
	}
}

ForEach ($line in $diagram) {
	ForEach ($element in $line) {
		If ($element -ge 2) {
			$overdos++
		}
	}
}

Write-Host $overdos