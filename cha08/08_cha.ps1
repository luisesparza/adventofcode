$TestData = $tfalse
$file = $TestData ? '..\src\08_test.txt' : '..\src\08_data.txt'
[string[]]$segment = @()
[int[]]$digits = @()
$count = 0

((Get-Content -Path $file).Split('|')) | ForEach-Object {
    if(-Not ($i % 2)) { $segment += $_.Split(' ') }
    ++$i
}

Foreach ($element in $segment) {
    $char = ($element | Measure-Object -Character).Characters
    if ($char -eq 2) {
        $count ++
        $digits += 1
    }
    elseif ($char -eq 3) {
        $count ++
        $digits += 7
    }
    elseif ($char -eq 4) {
        $count ++
        $digits += 4
    }
    elseif ($char -eq 7) {
        $count++
        $digits += 8
    }
    elseif ($char -eq 5) {
        if ([string]$element.contains('a')) {
            if ($element.contains("b")) {
                #Write-Host "3" $element
            }
            else {
                #Write-Host "2" $element
            }
        }
        else {
            #Write-Host "5" $element
        }
    }
    elseif ($char -eq 6) {
        if ($element -match ("g")) {
            if ($element -match ("a")) {
                #Write-Host "0" $element
            }
            else {
                #Write-Host "6" $element
            }
        }
        else {
            #Write-Host "9" $element
        }

    }
    else {$digits += ""}
}

Write-Host $count
