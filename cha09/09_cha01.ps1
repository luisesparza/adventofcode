$TestData = $false
$file = $TestData ? (Get-Content -Path '..\src\09_test.txt') : (Get-Content -Path '..\src\09_data.txt')
$lava = @()
$risk = 0

$file | Foreach-Object { $lava += @($_ -split "\D") }

For ($i=1; $i -lt $lava.length-1; $i++){
    For ($j=1; $j -lt $lava[$i].length-1; $j++) {
        If (($lava[$i][$j] -lt $lava[$i+1][$j]) -and ($lava[$i][$j] -lt $lava[$i-1][$j]) -and ($lava[$i][$j] -lt $lava[$i][$j+1]) -and ($lava[$i][$j] -lt $lava[$i][$j-1])) {            
            $risk += [convert]::ToInt32($lava[$i][$j], 10) + 1
        }
    }
}

For ($j=1; $j -lt $lava[0].length-1; $j++) {
    If (($lava[0][$j] -lt $lava[0][$j-1]) -and ($lava[0][$j] -lt $lava[0][$j+1]) -and ($lava[0][$j] -lt $lava[1][$j])) {
        $risk += [convert]::ToInt32($lava[0][$j], 10) + 1
    }
}

$a01 = $lava.length-1
For ($j=1; $j -lt $lava[$a01].length-1; $j++) {
    If (($lava[$a01][$j] -lt $lava[$a01][$j-1]) -and ($lava[$a01][$j] -lt $lava[$a01][$j+1]) -and ($lava[$a01][$j] -lt $lava[$a01-1][$j])) {
        $risk += [convert]::ToInt32($lava[$a01][$j], 10) + 1
    }
}

For ($i=1; $i -lt $lava.length-1; $i++) {
    If (($lava[$i][0] -lt $lava[$i-1][0]) -and ($lava[$i][0] -lt $lava[$i+1][0]) -and ($lava[$i][0] -lt $lava[$i][1])) {
        $risk += [convert]::ToInt32($lava[$i][0], 10) + 1
    }
}

$a02 = $lava[0].length-1
For ($i=1; $i -lt $lava.length-1; $i++){
    If (($lava[$i][$a02] -lt $lava[$i-1][$a02]) -and ($lava[$i][$a02] -lt $lava[$i+1][$a02]) -and ($lava[$i][$a02] -lt $lava[$i][$a02-1])) {
        $risk += [convert]::ToInt32($lava[$i][$a02], 10) + 1
    }
}

If (($lava[0][0] -lt $lava[0][1]) -and ($lava[0][0] -lt $lava[1][0])) {
    $risk += [convert]::ToInt32($lava[0][0], 10) + 1
}

If (($lava[$a01][0] -lt $lava[$a01][1]) -and ($lava[$a01][0] -lt $lava[$a01-1][0])) {
    $risk += [convert]::ToInt32($lava[$a01][0], 10) + 1
}

If (($lava[0][$a02] -lt $lava[1][$a02]) -and ($lava[0][$a02] -lt $lava[0][$a02-1])) {
    $risk += [convert]::ToInt32($lava[0][$a02], 10) + 1
}

If (($lava[$a01][$a02] -lt $lava[$a01-1][$a02]) -and ($lava[$a01][$a02] -lt $lava[$a01][$a02-1])) {
    $risk += [convert]::ToInt32($lava[$a01][$a02], 10) + 1
}

Write-Host $risk