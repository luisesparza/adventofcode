$TestData = $false
$file = $TestData ? '..\src\06_test.txt' : '..\src\06_data.txt'
[int[]]$matrix = (Get-Content -Path $file) -Split (',')
$dias = 80

function lanternfish {
	Param (
		[Parameter(Mandatory=$true)] $hijos
   )
	
	$count = 0
	ForEach ($e in $hijos) {
		if ($e -eq "0") {
		$count++
		}
	}

	For ($i=0; $i -lt $hijos.length; $i++) {
		$hijos[$i] -= 1
	}

	For ($l=0; $l -lt $count; $l++) {
		$hijos += 8
	}
	
	For ($m=0; $m -lt $hijos.length; $m++) {
		if ($hijos[$m] -eq -1) {
			$hijos[$m] = 6}
	}
	
	return $hijos
}

For ($d=0; $d -lt $dias; $d++) {
	$matrix = lanternfish $matrix
	
}

Write-Host $matrix.length