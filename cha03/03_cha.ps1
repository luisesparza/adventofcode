#$file = Get-Content ..\src\03_test.txt
$file = Get-Content ..\src\03_data.txt
$size = ($file | Select -First 1).length
$gamma = [object[]]::new($size)
$g0 = [object[]]::new($size)
$g1 = [object[]]::new($size)
$epsilon = [object[]]::new($size)
$oxygen = $file
$co2 = $file
$size = ($file | Select -First 1).length
 
ForEach ($line in $file) {
	For ($char = 0; $char -lt $line.length) {
		If ($line.substring($char,1) -eq 0) {
			$g0[$char] += 1
		}
		ElseIf ($line.substring($char,1) -eq 1) {
			$g1[$char] += 1
		}
		$char++
	}
}

For ($index = 0; $index -lt $gamma.length) {
	If ($g0[$index] -gt $g1[$index]) {
		$gamma[$index] = 0
	}
	ElseIf ($g0[$index] -lt $g1[$index]) {
		$gamma[$index] = 1
	}
	$index++
}

ForEach ($char in $gamma) {
	If ($char -eq 0) {
		$epsilon += 1
	}
	ElseIf ($char -eq 1) {
		$epsilon += 0
	}
}

$gamma = ($gamma | Out-String) -replace '\r*\n'
$epsilon = ($epsilon | Out-String) -replace '\r*\n'

$gamma = [Convert]::ToInt32($gamma,2)
$epsilon = [Convert]::ToInt32($epsilon,2)

Write-Host ($gamma * $epsilon)

function diagnostic {
	Param (
	 	[Parameter(Mandatory=$true)] $position,
		[Parameter(Mandatory=$true)] $choose,
		[Parameter(Mandatory=$true)] $matrix
	)
	$ceros = 0
	$unos = 0
	$win = 0
	$result = @()
	ForEach ($line in $matrix) {
		If ($line.Substring($position,1) -eq "0") {
			$ceros++
		}
		ElseIf ($line.Substring($position,1) -eq "1") {
			$unos++
		}
	}
	If ($choose -eq "ox") {
		If ($ceros -gt $unos) {
			$win = 0
		}
		ElseIf ($unos -gt $ceros) {
			$win = 1
		}
		ElseIf ($ceros -eq $unos) {
			$win = 2
		}
	}
	If ($choose -eq "co") {
		If ($ceros -lt $unos) {
			$win = 0
		}
		ElseIf ($unos -lt $ceros) {
			$win = 1
		}
		ElseIf ($ceros -eq $unos) {
			$win = 2
		}
	}
	If ($win -eq 2) {
		If ($choose -eq "ox") {
			$win = 1
		}
		ElseIf ($choose -eq "co") {
			$win = 0
		}
	}
	ForEach ($line in $matrix) {
		If ($line.Substring($position,1) -eq $win) {
			$result += $line
		}
	}
	return $result
}

For ($index = 0; $index -lt $size; $index++) {
	$oxygen = diagnostic $index "ox" $oxygen
	If ($oxygen.count -eq 1) {
		break
	}
}

For ($index = 0; $index -lt $size; $index++) {
	$co2 = diagnostic $index "co" $co2
	If ($co2.count -eq 1) {
		break
	}
}

$oxygen = [Convert]::ToInt32($oxygen,2)
$co2 = [Convert]::ToInt32($co2,2)

Write-Host ($oxygen * $co2)